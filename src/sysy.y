%code requires {
  #include <memory>
  #include <string>
  #include "main.h"
}

%{

#include <iostream>
#include <memory>
#include <string>
#include "main.h"

// 声明 lexer 函数和错误处理函数
int yylex();
void yyerror(std::unique_ptr<BaseAST> &ast, const char *s);

using namespace std;

%}

// 定义 parser 函数和错误处理函数的附加参数
// 我们需要返回一个字符串作为 AST, 所以我们把附加参数定义成字符串的智能指针
// 解析完成后, 我们要手动修改这个参数, 把它设置成解析得到的字符串
%parse-param { std::unique_ptr<BaseAST> &ast }

// yylval 的定义, 我们把它定义成了一个联合体 (union)
// 因为 token 的值有的是字符串指针, 有的是整数
// 之前我们在 lexer 中用到的 str_val 和 int_val 就是在这里被定义的
// 至于为什么要用字符串指针而不直接用 string 或者 unique_ptr<string>?
// 请自行 STFW 在 union 里写一个带析构函数的类会出现什么情况
%union {
  std::string *str_val;
  int int_val;
  char char_val;
  BaseAST *ast_val;
  std::vector<BaseAST*> *vec_val;
}

// lexer 返回的所有 token 种类的声明
// 注意 IDENT 和 INT_CONST 会返回 token 的值, 分别对应 str_val 和 int_val
%token INT RETURN CONST
%token <str_val> IDENT RELOP EQOP LANDOP LOROP
%token <int_val> INT_CONST

// 非终结符的类型定义
%type <ast_val> FuncDef FuncType Stmt Exp PrimaryExp UnaryExp AddExp MulExp RelExp EqExp LAndExp LOrExp
%type <int_val> Number
%type <char_val> UnaryOp

%type <str_val> BType
%type <ast_val> Decl ConstDecl ConstDef ConstInitVal Block BlockItem ConstExp LVal
%type <vec_val> ConstDefList BlockItemList

// 此外，更新了 Decl 与 Stmt
%type <ast_val> VarDecl VarDef InitVal
%type <vec_val> VarDefList

// Lv 5:
// 更新了 Stmt

%%

CompUnit
  : FuncDef {
    std::cerr << "CompUnit" << std::endl;
    auto comp_unit = make_unique<CompUnitAST>();
    comp_unit->func_def = unique_ptr<BaseAST>($1);
    ast = move(comp_unit);
  }
  ;

FuncDef
  : FuncType IDENT '(' ')' Block {
    std::cerr << "FuncDef" << std::endl;
    auto ast = new FuncDefAST();
    ast->func_type = unique_ptr<BaseAST>($1);
    ast->ident = *unique_ptr<string>($2);
    ast->block = unique_ptr<BaseAST>($5);
    $$ = ast;
  }
  ;

// 同上, 不再解释
FuncType
  : INT {
    std::cerr << "FuncType" << ' ' << "int" << std::endl;
    auto ast = new FuncTypeAST();
    ast->type = "int";
    $$ = ast;
  }
  ;

Stmt
  : RETURN Exp ';' {
    std::cerr << "Stmt ::= RETURN Exp ';'" << std::endl;
    auto ast = new StmtAST();
    ast->exp = unique_ptr<BaseAST>($2);
    ast->stmt_type = StmtAST::RETURN_EXP;
    $$ = ast;
  }
  | RETURN ';' {
    std::cerr << "Stmt ::= RETURN Exp ';'" << std::endl;
    auto ast = new StmtAST();
    ast->stmt_type = StmtAST::RETURN;
    $$ = ast;
  }
  | LVal '=' Exp ';' {
    std::cerr << "Stmt ::= LVal '=' Exp ';'" << std::endl;
    auto ast = new StmtAST();
    ast->lval = unique_ptr<BaseAST>($1);
    ast->exp = unique_ptr<BaseAST>($3);
    ast->stmt_type = StmtAST::LVAL_EQUAL_EXP;
    $$ = ast;
  }
  | Block {
    std::cerr << "Stmt ::= Block" << std::endl;
    auto ast = new StmtAST();
    ast->block = unique_ptr<BaseAST>($1);
    ast->stmt_type = StmtAST::BLOCK;
    $$ = ast;
  }
  | Exp ';' {
    std::cerr << "Stmt ::= Exp ';'" << std::endl;
    auto ast = new StmtAST();
    ast->exp = unique_ptr<BaseAST>($1);
    ast->stmt_type = StmtAST::EXP;
    $$ = ast;
  }
  | ';' {
    std::cerr << "Stmt ::= ';'" << std::endl;
    auto ast = new StmtAST();
    ast->stmt_type = StmtAST::SEMICOLON;
    $$ = ast;
  }
  ;

Exp
  : LOrExp {
    std::cerr << "Exp ::= LOrExp" << std::endl;
    auto ast = new ExpAST();
    ast->lor_exp = unique_ptr<BaseAST>($1);
    $$ = ast;
  }
  ;

// DeclAST() ?
Decl
  : ConstDecl {
    $$ = $1;
  }
  | VarDecl {
    $$ = $1;
  }
  ;

VarDecl
  : BType VarDefList ';' {
    auto ast = new VarDeclAST();
    ast->btype = *($1);
    ast->var_def_list = *($2);
    $$ = ast;
  }
  ;

VarDefList
  : VarDef {
    auto vec = new std::vector<BaseAST*>();
    vec->push_back($1);
    $$ = vec;
  }
  | VarDefList ',' VarDef {
    ($1)->push_back($3);
    $$ = $1;
  }
  ;

VarDef
  : IDENT {
    auto ast = new VarDefAST();
    ast->ident = *($1);
    ast->init_val = nullptr;
    $$ = ast;
  }
  | IDENT '=' InitVal {
    auto ast = new VarDefAST();
    ast->ident = *($1);
    ast->init_val = $3;
    $$ = ast;
  }
  ;

InitVal
  : Exp {
    $$ = $1;
  }
  ;

ConstDecl
  : CONST BType ConstDefList ';' {
    auto ast = new ConstDeclAST();
    ast->btype = *($2);
    ast->const_def_list = *($3);
    $$ = ast;
  }
  ;

ConstDefList
  : ConstDef {
    auto vec = new std::vector<BaseAST*>;
    vec->push_back($1);
    $$ = vec;
  }
  | ConstDefList ',' ConstDef {
    ($1)->push_back($3);
    $$ = $1;
  }
  ;

BType
  : INT {
    auto s = new std::string("int");
    $$ = s;
  }
  ;

ConstDef
  : IDENT '=' ConstInitVal
  {
    auto ast = new ConstDefAST();
    ast->ident = *($1);
    ast->const_init_val = $3;
    $$ = ast;
  }
  ;

ConstInitVal
  : ConstExp
  {
    $$ = $1;
  }
  ;

Block
  : '{' BlockItemList '}' {
    auto ast = new BlockAST();
    ast->block_item_list = *($2);
    $$ = ast;
  }
  ;

BlockItemList
  : {
    auto vec = new std::vector<BaseAST*>;
    $$ = vec;
  }
  | BlockItemList BlockItem {
    ($1)->push_back($2);
    $$ = $1;
  }
  ;

BlockItem
  : Decl {
    $$ = $1;
  }
  | Stmt {
    $$ = $1;
  }
  ;

LVal
  : IDENT {
    auto ast = new LValAST();
    ast->ident = *($1);
    $$ = ast;
  }
  ;

PrimaryExp
  : '(' Exp ')' {
    std::cerr << "PrimaryExp ::= '(' Exp ')'" << std::endl;
    auto ast = new PrimaryExpAST();
    ast->type = EXP;
    ast->exp = unique_ptr<BaseAST>($2);
    $$ = ast;
  }
  | Number {
    std::cerr << "PrimaryExp ::= Number" << std::endl;
    auto ast = new PrimaryExpAST();
    ast->type = NUMBER;
    ast->number = $1;
    $$ = ast;
  }
  | LVal {
    auto ast = new PrimaryExpAST();
    ast->type = LVAL;
    ast->lval = unique_ptr<BaseAST>($1);
    $$ = ast;
  }
  ;

ConstExp
  : Exp {
    auto ast = new ConstExpAST();
    ast->exp = unique_ptr<BaseAST>($1);
    $$ = ast;
  }
  ;

RelExp
  : AddExp {
    $$ = $1;
  }
  | RelExp RELOP AddExp {
    auto ast = new BinOpExpAST();
    ast->lhs_exp = unique_ptr<BaseAST>($1);
    ast->op = *($2);
    ast->rhs_exp = unique_ptr<BaseAST>($3);
    $$ = ast;
  }
  ;

EqExp
  : RelExp {
    $$ = $1;
  }
  | EqExp EQOP RelExp {
    auto ast = new BinOpExpAST();
    ast->lhs_exp = unique_ptr<BaseAST>($1);
    ast->op = *($2);
    ast->rhs_exp = unique_ptr<BaseAST>($3);
    $$ = ast;
  }
  ;

LAndExp
  : EqExp {
    $$ = $1;
  }
  | LAndExp LANDOP EqExp {
    auto ast = new LogOpExpAST();
    ast->lhs_exp = unique_ptr<BaseAST>($1);
    ast->op = *($2);
    ast->rhs_exp = unique_ptr<BaseAST>($3);
    $$ = ast;
  }
  ;

LOrExp
  : LAndExp {
    $$ = $1;
  }
  | LOrExp LOROP LAndExp {
    auto ast = new LogOpExpAST();
    ast->lhs_exp = unique_ptr<BaseAST>($1);
    ast->op = *($2);
    ast->rhs_exp = unique_ptr<BaseAST>($3);
    $$ = ast;
  }
  ;

AddExp
  : MulExp {
    std::cerr << "AddExp ::= MulExp" << std::endl;
    auto ast = new AddExpAST();
    ast->mul_exp = unique_ptr<BaseAST>($1);
    ast->op = '\0';
    $$ = ast;
  }
  | AddExp '+' MulExp {
    std::cerr << "AddExp ::= AddExp + MulExp" << std::endl;
    auto ast = new AddExpAST();
    ast->add_exp = unique_ptr<BaseAST>($1);
    ast->op = '+';
    ast->mul_exp = unique_ptr<BaseAST>($3);
    $$ = ast;
  }
  | AddExp '-' MulExp {
    std::cerr << "AddExp ::= AddExp - MulExp" << std::endl;
    auto ast = new AddExpAST();
    ast->add_exp = unique_ptr<BaseAST>($1);
    ast->op = '-';
    ast->mul_exp = unique_ptr<BaseAST>($3);
    $$ = ast;
  }
  ;

MulExp
  : UnaryExp {
    std::cerr << "MulExp ::= UnaryExp" << std::endl;
    auto ast = new MulExpAST();
    ast->unary_exp = unique_ptr<BaseAST>($1);
    ast->op = '\0';
    $$ = ast;
  }
  | MulExp '*' UnaryExp {
    std::cerr << "MulExp ::= MulExp * UnaryExp" << std::endl;
    auto ast = new MulExpAST();
    ast->mul_exp = unique_ptr<BaseAST>($1);
    ast->op = '*';
    ast->unary_exp = unique_ptr<BaseAST>($3);
    $$ = ast;
  }
  | MulExp '/' UnaryExp {
    std::cerr << "MulExp ::= MulExp / UnaryExp" << std::endl;
    auto ast = new MulExpAST();
    ast->mul_exp = unique_ptr<BaseAST>($1);
    ast->op = '/';
    ast->unary_exp = unique_ptr<BaseAST>($3);
    $$ = ast;
  }
  | MulExp '%' UnaryExp {
    std::cerr << "MulExp ::= MulExp % UnaryExp" << std::endl;
    auto ast = new MulExpAST();
    ast->mul_exp = unique_ptr<BaseAST>($1);
    ast->op = '%';
    ast->unary_exp = unique_ptr<BaseAST>($3);
    $$ = ast;
  }
  ;

Number
  : INT_CONST {
    std::cerr << "Number" << ' ' << $1 << std::endl;
    $$ = $1;
  }
  ;

UnaryExp
  : PrimaryExp {
    std::cerr << "UnaryExp ::= PrimaryExp" << std::endl;
    auto ast = new UnaryExpAST();
    ast->unary_op = '\0';
    ast->primary_exp = unique_ptr<BaseAST>($1);
    $$ = ast;
  }
  | UnaryOp UnaryExp {
    std::cerr << "UnaryExp ::= UnaryOp UnaryExp" << std::endl;
    auto ast = new UnaryExpAST();
    ast->unary_op = $1;
    ast->unary_exp = unique_ptr<BaseAST>($2);
    $$ = ast;
  }
  ;

UnaryOp
  : '+' {
    std::cerr << "UnaryOp" << ' ' << '+' << std::endl;
    $$ = '+';
  }
  | '-' {
    std::cerr << "UnaryOp" << ' ' << '-' << std::endl;
    $$ = '-';
  }
  | '!' {
    std::cerr << "UnaryOp" << ' ' << '!' << std::endl;
    $$ = '!';
  }
  ;
%%

// 定义错误处理函数, 其中第二个参数是错误信息
// parser 如果发生错误 (例如输入的程序出现了语法错误), 就会调用这个函数
void yyerror(unique_ptr<BaseAST> &ast, const char *s) {
  // if (ast) ast->Dump();
  cerr << "error: " << s << endl;
}
