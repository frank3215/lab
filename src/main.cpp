#include <cassert>
#include <cstdio>
#include <iostream>
#include <memory>
#include <string>
#include "main.h"
#include "koopa.h"

using namespace std;

int cnt;
symbol_table *current_symbol_table = nullptr;

symbol_table* find_symbol(std::string symbol) {
  symbol_table *t = current_symbol_table;
  while (t) {
    if (t->table.count(symbol)) return t;
    t = t->parent;
  }
  return nullptr;
}

// 声明 lexer 的输入, 以及 parser 函数
// 为什么不引用 sysy.tab.hpp 呢? 因为首先里面没有 yyin 的定义
// 其次, 因为这个文件不是我们自己写的, 而是被 Bison 生成出来的
// 你的代码编辑器/IDE 很可能找不到这个文件, 然后会给你报错 (虽然编译不会出错)
// 看起来会很烦人, 于是干脆采用这种看起来 dirty 但实际很有效的手段
extern FILE *yyin;
extern int yyparse(unique_ptr<BaseAST> &ast);

void Visit(const koopa_raw_program_t &program);
void Visit(const koopa_raw_slice_t &slice);
void Visit(const koopa_raw_function_t &func);
void Visit(const koopa_raw_basic_block_t &bb);
void Visit(const koopa_raw_value_t &value, int reg = 0);
void Visit(const koopa_raw_return_t &ret);
void Visit(const koopa_raw_binary_t &binary, int reg = 0);
void Visit(const koopa_raw_integer_t &integer, int reg = 0);

void Alloc(const koopa_raw_value_t &value);
void Load(const koopa_raw_load_t &load, const koopa_raw_value_t &value);
void Store(const koopa_raw_store_t &store, const koopa_raw_value_t &value);

// 访问 raw program
void Visit(const koopa_raw_program_t &program) {
  // 执行一些其他的必要操作
  cout << ".text" << endl;
  cout << ".globl main" << endl; // TODO: 推广至多函数的情况
  // 访问所有全局变量
  cerr << "Visit(program.values);" << endl;
  Visit(program.values);
  // 访问所有函数
  cerr << "Visit(program.funcs);" << endl;
  Visit(program.funcs);
}

// 访问 raw slice
void Visit(const koopa_raw_slice_t &slice) {
  cerr << "Visit(koopa_raw_slice_t): [" << slice.len << "]" << endl;
  for (size_t i = 0; i < slice.len; ++i) {
    auto ptr = slice.buffer[i];
    // 根据 slice 的 kind 决定将 ptr 视作何种元素
    switch (slice.kind) {
      case KOOPA_RSIK_FUNCTION:
        // 访问函数
        Visit(reinterpret_cast<koopa_raw_function_t>(ptr));
        break;
      case KOOPA_RSIK_BASIC_BLOCK:
        // 访问基本块
        Visit(reinterpret_cast<koopa_raw_basic_block_t>(ptr));
        break;
      case KOOPA_RSIK_VALUE:
        // 访问指令
        if (reinterpret_cast<koopa_raw_value_t>(ptr)->kind.tag == KOOPA_RVT_BINARY) {
          // 无副作用的式子，直接跳过，不进行处理。——对吗？WARN
          break;
        }
        Visit(reinterpret_cast<koopa_raw_value_t>(ptr));
        break;
      default:
        // 我们暂时不会遇到其他内容, 于是不对其做任何处理
        assert(false);
    }
  }
}

// 访问函数
void Visit(const koopa_raw_function_t &func) { // KOOPA_RSIK_FUNCTION
  cerr << "Visit(koopa_raw_function_t)" << endl;
  // 执行一些其他的必要操作
  cout << (func->name+1) << ":" << endl;
  // 访问所有基本块
  Visit(func->bbs);
}

// 访问基本块
void Visit(const koopa_raw_basic_block_t &bb) { // KOOPA_RSIK_BASIC_BLOCK
  cerr << "Visit(koopa_raw_basic_block_t)" << endl;
  // 执行一些其他的必要操作
  // ...
  // 访问所有指令
  Visit(bb->insts);
}

// 访问指令
void Visit(const koopa_raw_value_t &value, int reg) { // KOOPA_RSIK_VALUE
  cerr << "\tVisit(koopa_raw_value_t)" << endl;
  // 根据指令类型判断后续需要如何访问
  const auto &kind = value->kind; // koopa_raw_value_kind_t
  switch (kind.tag) {
    case KOOPA_RVT_RETURN:
      // 访问 return 指令
      Visit(kind.data.ret); // koopa_raw_return_t
      break;
    case KOOPA_RVT_INTEGER:
      // 访问 integer 指令
      Visit(kind.data.integer, reg); // koopa_raw_integer_t
      break;
    case KOOPA_RVT_BINARY:
      Visit(kind.data.binary, reg); // koopa_raw_binary_t
      break;
    case KOOPA_RVT_ALLOC:
      // 访问 (local) alloc 指令
      Alloc(value); // koopa_raw_global_alloc_t ;
    case KOOPA_RVT_LOAD:
      // 访问 load 指令
      Load(kind.data.load, value);
    case KOOPA_RVT_STORE:
      Store(kind.data.store, value);
    default:
      // 其他类型暂时遇不到
      std::cerr << kind.tag << std::endl;
      assert(false);
  }
}

// 访问对应类型指令的函数定义略
// 视需求自行实现

std::map<unsigned long long, int> memory;
int nowid;

void Alloc(const koopa_raw_value_t &value) { // KOOPA_RVT_ALLOC
  std::cerr << "alloc" << (unsigned long long)value << std::endl;
  memory[(unsigned long long)value] = nowid;
  nowid += 4;
}

void Load(const koopa_raw_load_t &load, const koopa_raw_value_t &value) {
  std::cerr << "load" << (unsigned long long)value << std::endl;
  int imm = memory[(unsigned long long)load.src];
  cout << "li t2, " << imm << endl;
  cout << "add t2, sp, t2" << endl;
  cout << "lw t3, 0(t2)" << endl;
  imm = memory[(unsigned long long)value] = nowid;
  nowid += 4;
  cout << "li t2, " << imm << endl;
  cout << "add t2, sp, t2" << endl;
  cout << "add t2, sp, t2" << endl;
  cout << "sw t3, 0(t2)" << endl;
}

void Store(const koopa_raw_store_t &store, const koopa_raw_value_t &value) {
  std::cerr << "store" << (unsigned long long)value << std::endl;
  std::cerr << (unsigned long long)store.value << " " << (unsigned long long)store.dest  << std::endl;
  int imm;
  if (store.value->kind.tag == KOOPA_RVT_INTEGER)
    cout << "li t3, " << store.value->kind.data.integer.value << endl;
  else {
    imm = memory[(unsigned long long)store.value];
    cout << "li t2," << imm << endl;
    cout << "add t2, sp, t2" << endl;
    cout << "lw t3, 0(t2)" << endl;
  }
  imm = memory[(unsigned long long)store.value];
  cout << "li t2, " << imm << endl;
  cout << "add t2, sp, t2" << endl;
  cout << "sw t3, 0(t2)" << endl;
}

void Visit(const koopa_raw_return_t &ret) { // KOOPA_RSIK_VALUE
  cerr << "Visit(koopa_raw_return_t)" << endl;
  // return 指令中, value 代表返回值
  const auto &ret_value = ret.value;
  const auto &ret_kind = ret_value->kind;
  // 示例程序中, ret_value 一定是一个 integer
  switch (ret_kind.tag) {
    case KOOPA_RVT_INTEGER:
      // 访问 integer 指令
      Visit(ret_kind.data.integer); // koopa_raw_integer_t
      cout << "mv a0, t0" << endl;
      cout << "ret" << endl;
      break;
    case KOOPA_RVT_BINARY:
      // 访问 binary 指令
      Visit(ret_kind.data.binary); // koopa_raw_binary_t 
      cout << "mv a0, t0" << endl;
      cout << "ret" << endl;
      break;
    default:
      cerr << ret_kind.tag << endl;
      // 其他类型暂时遇不到
      assert(false);
  }
}

// WARN 我假设二元运算的层数较少，直接使用
void Visit(const koopa_raw_binary_t &binary, int reg) { // KOOPA_RSIK_VALUE
  cerr << "Visit(koopa_raw_binary_t)" << endl;
  const auto &op = binary.op; // koopa_raw_binary_op_t
  const auto &lhs = binary.lhs; // koopa_raw_value_t
  const auto &rhs = binary.rhs; // koopa_raw_value_t

  switch (op) {
    case KOOPA_RBO_SUB: cerr << "SUB" << endl; break;
    case KOOPA_RBO_EQ: cerr << "EQ" << endl; break;
    case KOOPA_RBO_MUL: cerr << "MUL" << endl; break;
    case KOOPA_RBO_ADD: cerr << "ADD" << endl; break;
    case KOOPA_RBO_DIV: cerr << "DIV" << endl; break;
    case KOOPA_RBO_MOD: cerr << "MOD" << endl; break;
    default: assert(true); // 其他类型的输出懒得加了，后面会处理。
  }
  
  { // 统一处理
    cerr << "lhs " << flush;
    Visit(lhs, reg); // koopa_raw_value_t
    cerr << "rhs " << flush;
    Visit(rhs, reg + 1); // koopa_raw_value_t
  }

  // std::map<koopa_raw_binary_op_t, std::string> inst;
  // inst[KOOPA_RBO_SUB] = "sub";

  // 现在 t1 = lhs, t0 = rhs，直接计算，将结果保存在 t0 中
  switch (op) {
    case KOOPA_RBO_SUB:
      // Subtraction.
      cout << "sub t" << reg << ", t" << reg << ", t" << reg + 1 << endl;
      break;
    case KOOPA_RBO_EQ:
      // Equal to.
      cout << "xor t" << reg << ", t" << reg << ", t" << reg + 1 << endl;
      cout << "seqz t" << reg << ", t" << reg << endl;
      break;
    case KOOPA_RBO_NOT_EQ:
      // Not equal to.
      cout << "xor t" << reg << ", t" << reg << ", t" << reg + 1 << endl;
      cout << "snez t" << reg << ", t" << reg << endl;
      break;
    case KOOPA_RBO_MUL:
      // Multiplication.
      cout << "mul t" << reg << ", t" << reg << ", t" << reg + 1 << endl;
      break;
    case KOOPA_RBO_ADD:
      // Addition.
      cout << "add t" << reg << ", t" << reg << ", t" << reg + 1 << endl;
      break;
    case KOOPA_RBO_DIV:
      // Division.
      cout << "div t" << reg << ", t" << reg << ", t" << reg + 1 << endl;
      break;
    case KOOPA_RBO_MOD:
      // Modulo.
      cout << "rem t" << reg << ", t" << reg << ", t" << reg + 1 << endl;
      break;
    case KOOPA_RBO_GT:
      // Greater than.
      cout << "sgt t" << reg << ", t" << reg << ", t" << reg + 1 << endl;
      break;
    case KOOPA_RBO_LT:
      // Less than.
      cout << "slt t" << reg << ", t" << reg << ", t" << reg + 1 << endl;
      break;
    case KOOPA_RBO_GE:
      // Greater than or equal to.
      cout << "slt t" << reg << ", t" << reg << ", t" << reg + 1 << endl;
      cout << "seqz t" << reg << ", t" << reg << endl;
      break;
    case KOOPA_RBO_LE:
      // Less than or equal to.
      cout << "sgt t" << reg << ", t" << reg << ", t" << reg + 1 << endl;
      cout << "seqz t" << reg << ", t" << reg << endl;
      break;
    case KOOPA_RBO_AND:
      // Bitwise and.
      cout << "and t" << reg << ", t" << reg << ", t" << reg + 1 << endl;
      break;
    case KOOPA_RBO_OR:
      // Bitwise or.
      cout << "or t" << reg << ", t" << reg << ", t" << reg + 1 << endl;
      break;
    default:
      // 其他类型暂时遇不到
      assert(false);
  }
}

void Visit(const koopa_raw_integer_t &integer, int reg) { // KOOPA_RSIK_VALUE
  cerr << "Visit(koopa_raw_integer_t): " << integer.value << endl;
  // integer 中, value 代表整数的数值
  const auto &int_val = integer.value;
 // 示例程序中, 这个数值一定是 0
  cout << "li t" << reg << ", " << int_val << endl;
}

int main(int argc, const char *argv[]) {
  // 解析命令行参数. 测试脚本/评测平台要求你的编译器能接收如下参数:
  // compiler 模式 输入文件 -o 输出文件
  assert(argc == 5);
  auto mode = argv[1];
  auto input = argv[2];
  auto output = argv[4];

  // 打开输入文件, 并且指定 lexer 在解析的时候读取这个文件
  yyin = fopen(input, "r");
  assert(yyin);

  // 调用 parser 函数, parser 函数会进一步调用 lexer 解析输入文件的
  unique_ptr<BaseAST> ast;
  auto ret = yyparse(ast);
  assert(!ret);

  std::cerr << "OK" << std::endl;

  // 输出解析得到的 AST, 其实就是个字符串
  if (mode == std::string("-koopa")) {
    ast->Dump();
    cerr << endl;
    freopen(output, "w", stdout);
    string s;
    ast->Koopa(s);
    cout << s;
  } else {
    assert(mode == std::string("-riscv"));
    freopen(output, "w", stdout);
    string s;
    ast->Koopa(s);

    // 解析字符串 str, 得到 Koopa IR 程序
    koopa_program_t program;
    koopa_error_code_t ret = koopa_parse_from_string(s.c_str(), &program);
    assert(ret == KOOPA_EC_SUCCESS);  // 确保解析时没有出错
    // 创建一个 raw program builder, 用来构建 raw program
    koopa_raw_program_builder_t builder = koopa_new_raw_program_builder();
    // 将 Koopa IR 程序转换为 raw program
    koopa_raw_program_t raw = koopa_build_raw_program(builder, program);
    // 释放 Koopa IR 程序占用的内存
    koopa_delete_program(program);

    // 处理 raw program
    Visit(raw);
    // 处理完成, 释放 raw program builder 占用的内存
    // 注意, raw program 中所有的指针指向的内存均为 raw program builder 的内存
    // 所以不要在 raw program 处理完毕之前释放 builder
    koopa_delete_raw_program_builder(builder);
  }
  cout << endl;
  return 0;
}
