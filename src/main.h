#pragma once
#include <cassert>
#include <memory>
#include <iostream>
#include <map>
#include <vector>
#define TODO assert(1)
// #define assert 

struct symbol {
  bool is_const;
  int const_value;
  std::string var_name;
};

struct symbol_table;

struct symbol_table {
  symbol_table *parent;
  int depth;
  std::map<std::string, symbol> table;
};

extern int cnt;
extern symbol_table *current_symbol_table;

symbol_table* find_symbol(std::string symbol);

enum AST_t {
  EXP,
  NUMBER,
  LVAL
  // CONST_DECL,
  // PRIMARY_EXP,
  // UNARY_EXP
};

// 所有 AST 的基类
class BaseAST {
 public:
  AST_t type;
  int const_value;
  bool is_const;
  virtual ~BaseAST() = default;

  virtual void Dump() = 0;
  virtual std::string Koopa(std::string &) = 0;
};

// CompUnit 是 BaseAST
class CompUnitAST : public BaseAST { // 对应 Koopa IR 的 Program
 public:
  // 用智能指针管理对象
  std::unique_ptr<BaseAST> func_def;

  void Dump() {
    std::cerr << "CompUnitAST { ";
    std::cerr << std::flush;
    func_def->Dump();
    std::cerr << " }";
    std::cerr << std::flush;
  }

  std::string Koopa(std::string &s) {
    func_def->Koopa(s);
    return std::string();
  }
};

// FuncDef 也是 BaseAST
class FuncDefAST : public BaseAST { // 对应 Koopa IR 的 Function
 public:
  std::unique_ptr<BaseAST> func_type;
  std::string ident;
  std::unique_ptr<BaseAST> block;

  void Dump() {
    std::cerr << "FuncDefAST { ";
    std::cerr << std::flush;
    func_type->Dump();
    std::cerr << ", " << ident << ", ";
    block->Dump();
    std::cerr << " }";
    std::cerr << std::flush;
  }

  std::string Koopa(std::string &s) {
    s = s + "fun @" + ident + "(): ";
    func_type->Koopa(s);
    s = s + " {\n";
    s = s + "\%entry:\n";
    block->Koopa(s);
    s = s + "}";
    return std::string();
  }
};

class FuncTypeAST : public BaseAST {
 public:
  std::string type;

  void Dump() {
    std::cerr << "FuncTypeAST { ";
    std::cerr << type;
    std::cerr << " }";
    std::cerr << std::flush;
  }

  std::string Koopa(std::string &s) {
    assert(type == "int");
    s = s + "i32";
    return std::string();
  }
};

// Stmt        ::= "return" Exp ";";
class StmtAST : public BaseAST { // 对应 Koopa IR 的 Value
 public:
  enum StmtType {
    RETURN_EXP,
    RETURN,
    LVAL_EQUAL_EXP,
    BLOCK,
    EXP,
    SEMICOLON
  } stmt_type;
  std::unique_ptr<BaseAST> exp;
  std::unique_ptr<BaseAST> lval;
  std::unique_ptr<BaseAST> block;

  void Dump() {
    std::cerr << "StmtAST { " << std::flush;
    if (stmt_type == RETURN_EXP) {
      std::cerr << "return, " << std::flush;
      exp->Dump();
    } else if (stmt_type == LVAL_EQUAL_EXP) {
      lval->Dump();
      std::cerr << ", " << std::flush;
      exp->Dump();
    } else if (stmt_type == RETURN) {
      std::cerr << "return" << std::flush;
    } else if (stmt_type == BLOCK) {
      block->Dump();
    } else if (stmt_type == EXP) {
      exp->Dump();
    } else if (stmt_type == SEMICOLON) {
      std::cerr << ";" << std::flush;
    } else assert(false);
    std::cerr << " }" << std::flush;
  }

  std::string Koopa(std::string &s) {
    // std::cerr << s << std::endl;
    // std::string t = exp->Koopa(s);
    this->type = ::EXP;
    std::cerr << "Stmt type: " << stmt_type << std::endl;
    if (stmt_type == RETURN_EXP) {
      // return statement should have no side effects
      // std::string u;
      std::string t = exp->Koopa(s);
      std::cerr << "Stmt returning " << t << std::endl;
      assert(t[0] == '%' || t == std::to_string(exp->const_value));
      s = s + "ret " + t + "\n";
    } else if (stmt_type == LVAL_EQUAL_EXP) {
      // assignment statement always has side effects
      std::string t1 = exp->Koopa(s);
      assert(t1[0] == '%' || t1 == std::to_string(exp->const_value));
      std::string t2 = lval->Koopa(s);
      assert(t2[0] == '@');
      t2 = t2.substr(1);
      symbol_table * table = find_symbol(t2);
      assert(table);
      assert(!table->table[t2].is_const);
      t2 = "@" + table->table[t2].var_name;
      s = s + "store " + t1 + ", " + t2 + "\n";
    } else if (stmt_type == RETURN) {
      s = s + "ret\n";
    } else if (stmt_type == BLOCK) {
      block->Koopa(s);
    } else if (stmt_type == EXP) {
      exp->Koopa(s);
    } else if (stmt_type == SEMICOLON) {
      // do nothing.
    } else assert(false);
    return std::string(); // statement currently evaluates to nothing
  }
};

class ExpAST : public BaseAST {
 public:
  std::unique_ptr<BaseAST> lor_exp;

  void Dump() {
    std::cerr << "ExpAST { " << std::flush;
    lor_exp->Dump();
    std::cerr << " }" << std::flush;
  }

  std::string Koopa(std::string &s) {
    // std::cerr << s << std::endl;
    // std::string u;
    std::string t = lor_exp->Koopa(s);
    assert(t[0] == '%' || t == std::to_string(lor_exp->const_value));
    this->type = lor_exp->type;
    const_value = lor_exp->const_value;
    return t;
  }
};

// LOrExpAST & LAndExpAST
class LogOpExpAST : public BaseAST {
 private:
  std::map<std::string, std::string> inst;
 public:
  std::string op;
  std::unique_ptr<BaseAST> lhs_exp, rhs_exp; // LogOpExpAST or BinOpExpAST

  LogOpExpAST() : BaseAST(), op() {
      inst["&&"] = "and";
      inst["||"] = "or";
  }

  void Dump() {
    std::cerr << "LogOpExpAST { " << std::flush;
    assert(op.size());
    lhs_exp->Dump();
    std::cerr << ", " << op << ", " << std::flush;
    rhs_exp->Dump();
    std::cerr << " }" << std::flush;
  }

  std::string Koopa(std::string &s) {
    std::cerr << "LogOpExpAST { " << std::flush;
    assert(op.size());
    // std::string u1, u2;
    std::string t1 = lhs_exp->Koopa(s);
    assert(t1[0] == '%' || t1 == std::to_string(lhs_exp->const_value));
    std::cerr << ", " << op << ", " << std::flush;
    std::string t2 = rhs_exp->Koopa(s);
    assert(t2[0] == '%' || t2 == std::to_string(rhs_exp->const_value));
    if (t1[0] == '%' || t2[0] == '%') {
      std::string t3 = "%" + std::to_string(++cnt);
      std::string t4 = "%" + std::to_string(++cnt);
      s = s + t3 + " = ne " + t1 + ", 0\n";
      s = s + t4 + " = ne " + t2 + ", 0\n";
      s = s + "%" + std::to_string(++cnt) + " = " + inst[op] + " " + t3 + ", " + t4 + "\n";
      return "%" + std::to_string(cnt);
    }
    if (op == "&&") {
      const_value = lhs_exp->const_value && rhs_exp->const_value;
    } else if (op == "||") {
      const_value = lhs_exp->const_value || rhs_exp->const_value;
    } else {
      assert(false);
    }
    std::cerr << " }" << std::flush;
    return std::to_string(const_value);
  }
};

class BinOpExpAST : public BaseAST {
 private:
  std::map<std::string, std::string> inst;
 public:
  std::string op;
  std::unique_ptr<BaseAST> lhs_exp, rhs_exp; // BinOpExpAST or AddExpAST

  BinOpExpAST() : BaseAST(), op() {
      inst["<"] = "lt";
      inst[">"] = "gt";
      inst["<="] = "le";
      inst[">="] = "ge";
      inst["=="] = "eq";
      inst["!="] = "ne";
  }

  void Dump() {
    std::cerr << "BinOpExpAST { " << std::flush;
    assert(op.size());
    lhs_exp->Dump();
    std::cerr << ", " << op << ", " << std::flush;
    rhs_exp->Dump();
    std::cerr << " }" << std::flush;
  }

  std::string Koopa(std::string &s) {
    std::cerr << "BinOpExpAST { " << std::flush;
    assert(op.size());
    // std::string u1, u2;
    std::string t1 = lhs_exp->Koopa(s);
    assert(t1[0] == '%' || t1 == std::to_string(lhs_exp->const_value));
    std::cerr << ", " << op << ", " << std::flush;
    std::string t2 = rhs_exp->Koopa(s);
    assert(t2[0] == '%' || t2 == std::to_string(rhs_exp->const_value));
    if (t1[0] == '%' || t2[0] == '%') {
      s = s + "%" + std::to_string(++cnt) + " = " + inst[op] + " " + t1 + ", " + t2 + "\n";
      return "%" + std::to_string(cnt);
    }
    if (op == "<") {
      const_value = lhs_exp->const_value < rhs_exp->const_value;
    } else if (op == ">") {
      const_value = lhs_exp->const_value > rhs_exp->const_value;
    } else if (op == "<=") {
      const_value = lhs_exp->const_value <= rhs_exp->const_value;
    } else if (op == ">=") {
      const_value = lhs_exp->const_value >= rhs_exp->const_value;
    } else if (op == "==") {
      const_value = lhs_exp->const_value == rhs_exp->const_value;
    } else if (op == "!=") {
      const_value = lhs_exp->const_value != rhs_exp->const_value;
    } else {
      assert(false);
    }
    std::cerr << " }" << std::flush;
    return std::to_string(const_value);
  }
};

// AddExp      ::= MulExp | AddExp ("+" | "-") MulExp;
class AddExpAST : public BaseAST {
 public:
  char op;
  std::unique_ptr<BaseAST> add_exp;
  std::unique_ptr<BaseAST> mul_exp;

  void Dump() {
    std::cerr << "AddExpAST { " << std::flush;
    if (op) {
      add_exp->Dump();
      std::cerr << ", " << op << ", " << std::flush;
      mul_exp->Dump();
    } else {
      mul_exp->Dump();
    }
    std::cerr << " }" << std::flush;
  }

  std::string Koopa(std::string &s) {
    std::cerr << "AddExpAST { " << std::flush;
    if (!op) {
      // std::string u;
      std::string t = mul_exp->Koopa(s);
      assert(t[0] == '%' || t == std::to_string(mul_exp->const_value));
      this->type = mul_exp->type;
      const_value = mul_exp->const_value;
      std::cerr << " }" << std::flush;
      return t;
    } else {
      // std::string u1, u2;
      std::string t1 = add_exp->Koopa(s);
      assert(t1[0] == '%' || t1 == std::to_string(add_exp->const_value));
      std::cerr << ", " << op << ", " << std::flush;
      std::string t2 = mul_exp->Koopa(s);
      assert(t2[0] == '%' || t2 == std::to_string(mul_exp->const_value));
      std::cerr << " }" << std::flush;
      switch (op) {
        case '+':
          std::cerr << t1 << " " << t2 << std::endl;
          if (t1[0] == '%' || t2[0] == '%') {
            s = s + "%" + std::to_string(++cnt) + " = add " + t1 + ", " + t2 + "\n";
            // std::cerr << "%" + std::to_string(cnt) + " = add " + t1 + ", " + t2 + "\n";
            return "%" + std::to_string(cnt);
          }
          const_value = add_exp->const_value + mul_exp->const_value;
          return std::to_string(const_value);
        case '-':
          if (t1[0] == '%' || t2[0] == '%') {
            s = s + "%" + std::to_string(++cnt) + " = sub " + t1 + ", " + t2 + "\n";
            return "%" + std::to_string(cnt);
          }
          const_value = add_exp->const_value - mul_exp->const_value;
          return std::to_string(const_value);
        default:
          assert(0);
      }
    }
  }
};

// MulExp      ::= UnaryExp | MulExp ("*" | "/" | "%") UnaryExp;
class MulExpAST : public BaseAST {
 public:
  char op;
  std::unique_ptr<BaseAST> mul_exp;
  std::unique_ptr<BaseAST> unary_exp;

  void Dump() {
    std::cerr << "MulExpAST { " << std::flush;
    if (op) {
      mul_exp->Dump();
      std::cerr << ", " << op << ", " << std::flush;
      unary_exp->Dump();
    } else {
      unary_exp->Dump();
    }
    std::cerr << " }" << std::flush;
  }

  std::string Koopa(std::string &s) {
    // std::cerr << s << std::endl;
    if (!op) {
      // std::string u;
      std::string t = unary_exp->Koopa(s);
      assert(t[0] == '%' || t == std::to_string(unary_exp->const_value));
      this->type = unary_exp->type;
      const_value = unary_exp->const_value;
      return t;
    } else {
      // std::string u1, u2;
      std::string t1 = mul_exp->Koopa(s);
      assert(t1[0] == '%' || t1 == std::to_string(mul_exp->const_value));
      std::string t2 = unary_exp->Koopa(s);
      assert(t2[0] == '%' || t2 == std::to_string(unary_exp->const_value));
      switch (op) {
        case '*':
          if (t1[0] == '%' || t2[0] == '%') {
            s = s + "%" + std::to_string(++cnt) + " = mul " + t1 + ", " + t2 + "\n";
            return "%" + std::to_string(cnt);
          }
          const_value = mul_exp->const_value * unary_exp->const_value;
          return std::to_string(const_value);
        case '/':
          if (t1[0] == '%' || t2[0] == '%') {
            s = s + "%" + std::to_string(++cnt) + " = div " + t1 + ", " + t2 + "\n";
            return "%" + std::to_string(cnt);
          }
          const_value = mul_exp->const_value / unary_exp->const_value;
          return std::to_string(const_value);
        case '%':
          if (t1[0] == '%' || t2[0] == '%') {
            s = s + "%" + std::to_string(++cnt) + " = mod " + t1 + ", " + t2 + "\n";
            return "%" + std::to_string(cnt);
          }
          const_value = mul_exp->const_value % unary_exp->const_value;
          return std::to_string(const_value);
        default:
          assert(0);
      }
    }
  }
};

// PrimaryExp    ::= "(" Exp ")" | LVal | Number;
class PrimaryExpAST : public BaseAST {
 public:
  int number;
  std::unique_ptr<BaseAST> exp;
  std::unique_ptr<BaseAST> lval;

  void Dump() {
    std::cerr << "PrimaryExpAST { ";
    std::cerr << std::flush;
    if (type == NUMBER) std::cerr << number;
    else if (type == EXP) exp->Dump();
    else if (type == LVAL) lval->Dump();
    else assert(false);
    std::cerr << " }";
    std::cerr << std::flush;
  }

  std::string Koopa(std::string &s) {
    // std::cerr << s << std::endl;
    if (type == NUMBER) {
      const_value = number;
      return std::to_string(const_value);
    } else if (type == EXP) {
      // std::string u;
      exp->Koopa(s);
      const_value = exp->const_value;
      return std::to_string(const_value);
    } else if (type == LVAL) {
      std::string t = lval->Koopa(s);
      assert(t[0] == '@');
      t = t.substr(1);
      symbol_table *table = find_symbol(t);
      assert(table);
      if (table->table[t].is_const) {
        std::cerr << t << " is const" << std::endl;
        const_value = table->table[t].const_value;
        return std::to_string(const_value);
      } else {
        std::cerr << t << " is not const" << std::endl;
        s = s + "%" + std::to_string(++cnt) + " = load @" + table->table[t].var_name + "\n";
        return "%" + std::to_string(cnt);
      }
    }
    else assert(0);
  }
};

// UnaryExp    ::= PrimaryExp | UnaryOp UnaryExp;
class UnaryExpAST : public BaseAST {
 public:
  std::unique_ptr<BaseAST> primary_exp;
  std::unique_ptr<BaseAST> unary_exp;
  char unary_op;

  // AST_t type;

  void Dump() {
    std::cerr << "UnaryExp { ";
    std::cerr << unary_op << ", ";
    std::cerr << std::flush;
    if (unary_op) unary_exp->Dump();
    else primary_exp->Dump();
    std::cerr << " }";
    std::cerr << std::flush;
  }

  std::string Koopa(std::string &s) {
    // std::cerr << s << std::endl;
    if (!unary_op) {
      // std::string u;
      std::string t = primary_exp->Koopa(s);
      assert(t[0] == '%' || t == std::to_string(primary_exp->const_value));
      this->type = primary_exp->type;
      const_value = primary_exp->const_value;
      std::cerr << "UnaryExp returning " << t << std::endl;
      return t;
    } else {
      // std::string u;
      std::string t = unary_exp->Koopa(s);
      assert(t[0] == '%' || t == std::to_string(unary_exp->const_value));
      type = EXP;
      switch (unary_op) {
        case '+':
          if (t[0] == '%') {
            return t;
          }
          std::cerr << '+' << std::endl;
          const_value = unary_exp->const_value;
          return std::to_string(const_value);
        case '-':
          if (t[0] == '%') {
            s = s + "%" + std::to_string(++cnt) + " = sub 0, " + t + "\n";
            return "%" + std::to_string(cnt);
          }
          const_value = -unary_exp->const_value;
          std::cerr << '-' << std::endl;
          return std::to_string(const_value);
        case '!':
          if (t[0] == '%') {
            s = s + "%" + std::to_string(++cnt) + " = eq 0, " + t + "\n";
            return "%" + std::to_string(cnt);
          }
          const_value = !unary_exp->const_value;
          std::cerr << '!' << std::endl;
          return std::to_string(const_value);
        default:
          assert(0);
      }
    }
  }
};
// ...

// Decl          ::= ConstDecl;
// class DeclAST : public BaseAST {
//  public:
//   std::unique_ptr<BaseAST> const_decl;

//   void Dump() {
//     std::cerr << "DeclAST { " << std::flush;
//     const_decl->Dump();
//     std::cerr << " }" << std::flush;
//   }

//   std::string Koopa(std::string &s) {
//     std::string t;
//     const_decl->Koopa(t);
//     return "";
//   }
// };

// ConstDecl     ::= "const" BType ConstDef {"," ConstDef} ";";
class ConstDeclAST : public BaseAST {
 public:
  std::string btype;
  std::vector<BaseAST*> const_def_list;

  void Dump() {
    std::cerr << "ConstDecl { " << btype << ", " << std::flush;
    assert(const_def_list.size());
    const_def_list[0]->Dump();
    for (int i = 1; i < (int)const_def_list.size(); ++i) {
      std::cerr << ", " << std::flush;
      const_def_list[i]->Dump();
    }
    std::cerr << " }" << std::flush;
  }

  std::string Koopa(std::string &s) {
    for (int i = 0; i < (int)const_def_list.size(); ++i) {
      // std::string u;
      const_def_list[i]->Koopa(s);
    }
    return "";
  }
};

// ConstDef      ::= IDENT "=" ConstInitVal;
class ConstDefAST : public BaseAST {
 public:
  std::string ident;
  BaseAST * const_init_val;
  void Dump() {
    std::cerr << "ConstDef { " << ident << ", " << std::flush;
    const_init_val->Dump();
    std::cerr << " }" << std::flush;
  }

  std::string Koopa(std::string &s) {
    assert(!current_symbol_table->table.count(ident));
    // std::string u;
    std::string t = const_init_val->Koopa(s);
    assert(t[0] != '%' && t == std::to_string(const_init_val->const_value));
    current_symbol_table->table[ident].is_const = true;
    current_symbol_table->table[ident].const_value = const_init_val->const_value;
    return "";
  }
};

// in Decl (in BlockItem)
// VarDecl       ::= BType VarDef {"," VarDef} ";";
// has side effects
class VarDeclAST : public BaseAST {
 public:
  std::string btype;
  std::vector<BaseAST*> var_def_list;

  void Dump() {
    std::cerr << "VarDecl { " << btype << ", " << std::flush;
    assert(var_def_list.size());
    var_def_list[0]->Dump();
    for (int i = 1; i < (int)var_def_list.size(); ++i) {
      std::cerr << ", " << std::flush;
      var_def_list[i]->Dump();
    }
    std::cerr << " }" << std::flush;
  }

  std::string Koopa(std::string &s) {
    for (int i = 0; i < (int)var_def_list.size(); ++i) {
      std::string t = var_def_list[i]->Koopa(s);
    }
    return "";
  }
};

// VarDef        ::= IDENT | IDENT "=" InitVal;
class VarDefAST : public BaseAST {
 public:
  std::string ident;
  BaseAST * init_val;
  void Dump() {
    std::cerr << "ConstDef { " << ident << ", " << std::flush;
    if (init_val) init_val->Dump();
    std::cerr << " }" << std::flush;
  }

  std::string Koopa(std::string &s) {
    assert(!current_symbol_table->table.count(ident));
    current_symbol_table->table[ident].is_const = false;
    current_symbol_table->table[ident].var_name = ident;
    std::string v = "@" + ident + "_" + std::to_string(current_symbol_table->depth);
    current_symbol_table->table[ident].var_name = v.substr(1);
    s = s + v + " = alloc i32\n";
    if (init_val) {
      std::string t = init_val->Koopa(s);
      if (t[0] != '%') { // init_val is const
        assert(t == std::to_string(init_val->const_value));
      }
      s = s + "store " + t + ", " + v + "\n";
    }
    return v;
  }
};

// Block         ::= "{" {BlockItem} "}";
// has side effects
class BlockAST : public BaseAST {
 public:
  std::vector<BaseAST*> block_item_list; // Decl (ConstDeclAST or VarDeclAST) or Stmt
  void Dump() {
    std::cerr << "BlockAST { " << std::flush;
    for (int i = 0; i < (int)block_item_list.size(); ++i) {
      block_item_list[i]->Dump();
      if (i+1 < (int)block_item_list.size())
        std::cerr << ", " << std::flush;
    }
    std::cerr << " }" << std::flush;
  }

  std::string Koopa(std::string &s) {
    symbol_table *current_block_table = new symbol_table();
    current_block_table->parent = current_symbol_table;
    if (current_symbol_table) {
      current_block_table->depth = current_symbol_table->depth+1;
    } else {
      current_block_table->depth = 0;
    }
    current_symbol_table = current_block_table;
    for (int i = 0; i < (int)block_item_list.size(); ++i) {
      block_item_list[i]->Koopa(s);
    }
    current_symbol_table = current_block_table->parent;
    delete current_block_table;
    return "";
  }
};

class LValAST : public BaseAST {
 public:
  std::string ident;
  void Dump() {
    std::cerr << "LVal { " << std::flush;
    std::cerr << ident << std::flush;
    std::cerr << " }" << std::flush;
  }

  std::string Koopa(std::string &s) {
    return "@" + ident;
    // symbol_table * table = find_symbol(ident);
    // assert(table);
    // return "@" + ident + "_" + std::to_string(table->depth);
  }
};

class ConstExpAST : public BaseAST {
 public:
  std::unique_ptr<BaseAST> exp;
  void Dump() {
    std::cerr << "ConstExp { " << std::flush;
    exp->Dump();
    std::cerr << " }" << std::flush;
  }

  std::string Koopa(std::string &s) {
    std::string u;
    exp->Koopa(s);
    const_value = exp->const_value;
    // this->type = CONST_DECL;
    return std::to_string(const_value);
  }
};